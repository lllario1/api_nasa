//
//  NetworkingProvider.swift
//  Api_Nasa
//
//  Created by Mario Valencia Morales on 11/9/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class observer: ObservableObject {
    @Published var datas = [item]()
    
    init(){
        AF.request("https://images-api.nasa.gov/search?q=apollo%2011").responseData { (data) in
            let json = try! JSON(data: data.data!)
            
            var linkImage: String = ""
            
            for i in json {
                for j in i.1["items"] {
                    for k in j.1["links"]{
                        if k.1["href"].stringValue.suffix(4) == ".jpg" {
                            linkImage = k.1["href"].stringValue
                            linkImage = k.1["href"].stringValue
                            linkImage = linkImage.replacingOccurrences(of: " ", with: "%20")
                        }
                    }
                    
                    for l in j.1["data"]{
                        self.datas.append(item(href: linkImage, title: l.1["title"].stringValue, description: l.1["description"].stringValue))
                    }
                    
                }
            }
        }
    }
}
