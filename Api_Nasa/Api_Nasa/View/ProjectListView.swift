//
//  ProjectListView.swift
//  Api_Nasa
//
//  Created by Mario Valencia Morales on 11/9/21.
//

import SwiftUI
import SDWebImageSwiftUI

final class ItemsModelData: ObservableObject {
    @Published var items = observer()
}

struct ProjectListView: View {
    @EnvironmentObject var itemsModelData: ItemsModelData
    @State private var showFavorites = false
    
    private var filteredItems: [item]{
        return itemsModelData.items.datas.filter { item in
            return !showFavorites || item.favorite
        }
    }
    
    var body: some View {
        NavigationView {
            VStack{
                Toggle(isOn: $showFavorites) {
                    Text("Show Favorites")
                }.padding()
                
                let withIndex = filteredItems.enumerated().map({ $0 })
                List(withIndex, id: \.element.id) { index, i in
                    NavigationLink(
                        destination: ProjectDetailView(item: item(href: i.href, title: i.title, description: i.description, favorite: i.favorite), favorite: $itemsModelData.items.datas[index].favorite)){
                            ItemCell(item: i)
                    }
                }
            }.navigationBarTitle("Project Apollo")
        }
    }
}

struct ItemCell: View {
    let item: item
    
    var body: some View {
        HStack (spacing: 20) {
            AnimatedImage(url: URL(string: item.href ?? "")).resizable()
                .scaledToFit()
                .frame(height: 70)
                .cornerRadius(4)
                .padding(.vertical, 10)
            
            Text(item.title ?? "").fontWeight(.semibold)
                .lineLimit(2)
                .minimumScaleFactor(0.5)
            
            Spacer()
            if item.favorite {
                Image(systemName: "star.fill").foregroundColor(.yellow)
            }
        }
    }
}

struct ProjectListView_Previews: PreviewProvider {
    static var previews: some View {
        ProjectListView().environmentObject(ItemsModelData()).environment(\.colorScheme, .dark)
    }
}
