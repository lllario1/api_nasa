//
//  ProjectDetailView.swift
//  Api_Nasa
//
//  Created by Mario Valencia Morales on 11/9/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct ProjectDetailView: View {
    
    let item: item
    @Binding var favorite: Bool
    
    var body: some View {
        VStack (spacing: 20){
            
            AnimatedImage(url: URL(string: item.href!))
                .resizable()
                .scaledToFit()
                .frame(height: 250)
                .cornerRadius(20)
            
            HStack{
                Text(item.title!).fontWeight(.semibold)
                .font(.body)
                .padding()
                
                Button {
                    favorite.toggle()
                } label: {
                    if favorite {
                        Image(systemName: "star.fill").foregroundColor(.yellow)
                    } else {
                        Image(systemName: "star").foregroundColor(.gray)
                    }
                    
                }
            }
            
            Text(item.description!)
                .font(.body)
                .padding()
            
            Spacer()
        }
    }
}

struct ProjectDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ProjectDetailView(item: item.init(href: nil, title: nil, description: nil, favorite: false), favorite: .constant(false)).environment(\.colorScheme, .dark)
    }
}
