//
//  ContentView.swift
//  Api_Nasa
//
//  Created by Mario Valencia Morales on 11/8/21.
//

import SwiftUI
import CoreData

struct ContentView: View {
   
    var body: some View {
        ProjectListView().environmentObject(ItemsModelData())
    } 
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
