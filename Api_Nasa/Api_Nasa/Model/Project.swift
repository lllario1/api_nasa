//
//  Project.swift
//  Api_Nasa
//
//  Created by Mario Valencia Morales on 11/9/21.
//

import Foundation
import SwiftUI

struct item: Identifiable {
    let id = UUID()
    let href: String?
    let title: String?
    let description: String?
    var favorite: Bool = false
}
