//
//  Api_NasaApp.swift
//  Api_Nasa
//
//  Created by Mario Valencia Morales on 11/8/21.
//

import SwiftUI

@main
struct Api_NasaApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
